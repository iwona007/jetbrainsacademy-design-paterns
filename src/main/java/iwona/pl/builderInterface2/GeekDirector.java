package iwona.pl.builderInterface2;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder Director.
 */
public class GeekDirector {
    public void buildAdmin(Builder builder) {
        builder.setType("Admin");
        ArrayList<String> languages = new ArrayList<>();
        languages.add("Perl");
        languages.add("PowerShell");
        builder.setLanguages(languages);
        builder.setExperience(10);
    }

    public void buildBackend(Builder builder) {
        builder.setType("Backend");
        List<String> languages = new ArrayList<>();
        languages.add("Python");
        languages.add("PHP");
        builder.setLanguages(languages);
        builder.setExperience(5);
    }

    public void buildRockStar(Builder builder){
        builder.setType("Rockstar");
        List<String> languages = new ArrayList<>();
        languages.add("Java");
        languages.add("Kotlin");
        languages.add("Scala");
        languages.add("Angular");
        builder.setLanguages(languages);
        builder.setExperience(20);
    }
}
