package iwona.pl.builderInterface2;

import java.util.Scanner;

public class GeekClient {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final String geekName = scanner.nextLine();
        final String geekType = scanner.nextLine();
        scanner.close();

        GeekDirector geekDirector = new GeekDirector();
        GeekBuilder geekBuilder = new GeekBuilder();

        Geek geek = null;
        if("Rockstar".equals(geekType)){
            geekDirector.buildRockStar(geekBuilder);
            geek = geekBuilder.getResult();
        } else if ("Backend".equals(geekType)){
            geekDirector.buildBackend(geekBuilder);
            geek = geekBuilder.getResult();
        } else if ("Admin".equals(geekType)){
            geekDirector.buildAdmin(geekBuilder);
            geek = geekBuilder.getResult();
        } else {
            System.out.println("Error");
            return;
        }
        System.out.println("Geek " + geekName + "created.");
        System.out.println(geek);
    }
}
