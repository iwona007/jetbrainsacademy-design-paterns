package iwona.pl.singleton;
/*
Complete the given SimpleCounter class as eager (not lazy) singleton. It should contain the following parts:
    a private no-argument constructor;
    a private static field instance that holds the created instance;
    a public static method getInstance without arguments, it returns the same instance of the class;
    a public integer non-static field counter.
You will not read input or print output in this problem.
If your solution is wrong, you will get a compile-time error or a runtime exception.
 */
public class SimpleCounter {

    private static SimpleCounter instance = new SimpleCounter();
    public int counter = 0;
    private SimpleCounter(){}

    public static SimpleCounter getInstance(){
        return instance;
    }


}
