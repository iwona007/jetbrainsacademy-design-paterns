package iwona.pl.commandTV;
/*
this is a receiver 2
 */
public class Channel {

    private int chanelNumber;

    public Channel(int chanelNumber) {
        this.chanelNumber = chanelNumber;
    }

    void changeChannel() {
        System.out.println("Channel was changed to: " + chanelNumber);
    }
}
