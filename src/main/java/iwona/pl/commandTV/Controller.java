package iwona.pl.commandTV;

/**
 * this is invoker
 */
public class Controller {
    private Command command;

    public void setCommand(Command command){
        this.command = command;
    }

    public void executeCommand(){
        command.execute();
    }
}
