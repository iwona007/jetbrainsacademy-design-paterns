package iwona.pl.commandTV;

import java.util.Scanner;

/**
 * this is a client
 */
public class Client {
    public static void main(String[] args) {

        Controller invoker = new Controller();
        Tv receiver = new Tv();



        Command changeChannel;
        int[] channelList = new int[3];

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            channelList[i] = scanner.nextInt();
        }


        Command turnOnTv = new TurnOnCommand(receiver);
        invoker.setCommand(turnOnTv);
        invoker.executeCommand();

        for (int i = 0; i <3 ; i++) {
            Channel receiver2 = new Channel(channelList[i]);
            changeChannel = new ChangeChannelCommand(receiver2);
            invoker.setCommand(changeChannel);
            invoker.executeCommand();
        }



        Command turnOff = new TurnOffCommand(receiver);
        invoker.setCommand(turnOff);
        invoker.executeCommand();


    }
}
