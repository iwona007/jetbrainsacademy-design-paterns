package iwona.pl.commandTV;

public class ChangeChannelCommand implements Command {

   private Channel channel;

    public ChangeChannelCommand(Channel channel) {
        this.channel = channel;
    }

    @Override
    public void execute() {
        channel.changeChannel();
    }
}
