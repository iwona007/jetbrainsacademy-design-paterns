package iwona.pl.commandTV;

/**
 * this is a receiver
 */
public class Tv {
    void turnOn() {
        System.out.println("Turning on the TV");
    }

    void turnOff() {
        System.out.println("Turning off the TV");
    }
}
