package iwona.pl.commandTV;

/*
 * suppose you are building a remote controller application. It performs three commands. turning on the TV,
 * changing the channel to a specific number, turning off the TV. Use the command pattern to implement this application.
 * Use the following guidelines.
 * Don't change the provided code.
 * The first command to execute is to turn on the TV. TurnOn command will print Turning on the TV.
 * Next change channels to the inputs given by the user.  The user will give only three inputs.
 * ChangeChannel command will print Channel was changed to X. X is the user given number.
 * The last command is to turn off the TV. TurnOff command will print Turning off the TV.
Sample Input 1:
 4 7 12
 *
Sample Output 1:
 * Turning on the TV
 * Channel was changed to 4
 * Channel was changed to 7
 * Channel was changed to 12
 * Turning off the TV
 */
public interface Command {
    void execute();
}
