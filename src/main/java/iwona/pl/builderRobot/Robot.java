package iwona.pl.builderRobot;

public class Robot {
    private String CPU;
    private int legs;
    private int hands;
    private int eyes;

    public Robot(String CPU, int legs, int hands, int eyes) {
        this.CPU = CPU;
        this.legs = legs;
        this.hands = hands;
        this.eyes = eyes;
    }

   public static class RobotBuilder {
       private String CPU;
       private int legs;
       private int hands;
       private int eyes;

       public RobotBuilder() {
       }

       public RobotBuilder setCPU(String CPU){
           this.CPU = CPU;
           return this;
       }

       public RobotBuilder setLegs(int legs){
           this.legs = legs;
           return this;
       }

       public RobotBuilder setHands(int hands){
           this.hands = hands;
           return this;
       }

       public RobotBuilder setEyes(int eyes){
           this.eyes = eyes;
           return this;
       }

       public Robot build(){
           return new Robot(CPU, legs, hands, eyes);
       }
   }

    @Override
    public String toString() {
        return "Robot{" +
                "CPU='" + CPU + '\'' +
                ", legs=" + legs +
                ", hands=" + hands +
                ", eyes=" + eyes +
                '}';
    }
}
