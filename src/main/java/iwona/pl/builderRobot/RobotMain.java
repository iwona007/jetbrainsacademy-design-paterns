package iwona.pl.builderRobot;

import java.util.Scanner;

public class RobotMain {
    public static void main(String[] args) {

        final Scanner scanner = new Scanner(System.in);
        final Robot.RobotBuilder robotBuilder = new Robot.RobotBuilder();
        /* Set CPU */
        robotBuilder.setCPU("Intel Core i5");
        /* Would like to set legs? */
        int legs = scanner.nextInt();
        /* Would like to set hands? */
        int hands = scanner.nextInt();
        /* Would like to set eyes? */
        int eyes = scanner.nextInt();

        Robot robot = /* write your code here */
                robotBuilder
                        .setLegs(legs)
                        .setHands(hands)
                        .setEyes(eyes)
                        .build();

        System.out.println(robot);
        scanner.close();
    }
}

