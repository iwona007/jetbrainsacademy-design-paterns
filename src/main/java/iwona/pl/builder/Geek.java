package iwona.pl.builder;

/*
Given the Builder pattern classes, implement the GeekBuilder method to compile the program and output the created geek.
Please, do not change the provided code of in class TestDrive
Sample Input 1: Garry

Sample Output 1:
Geek Garry created.
Type : Admin
Languages : [Perl, PowerShell]
Experience : 10 years
 */

import java.util.List;
/**
 * ConcreteComponent - Geek.
 **/
public class Geek {

    private String type;
    private List<String> languages;
    private int experience;

    Geek(String type, List<String> languages, int experience) {
        this.type = type;
        this.languages = languages;
        this.experience = experience;
    }

    public static class GeekBuilder {
        private String type;
        private List<String> languages;
        private int experience;

        public GeekBuilder() {
        }

        GeekBuilder setType(String type) {
            this.type = type;
            return this;
        }

        GeekBuilder setLanguage(List<String> language) {
            this.languages = language;
            return this;
        }

        GeekBuilder setExperience(int experience) {
            this.experience = experience;
            return this;
        }

        public Geek build() {
            return new Geek (type, languages, experience);
        }
    }

    @Override
    public String toString() {
        return "Geek{" +
                "type='" + type + '\'' +
                ", languages=" + languages +
                ", experience=" + experience +
                '}';
    }
}
