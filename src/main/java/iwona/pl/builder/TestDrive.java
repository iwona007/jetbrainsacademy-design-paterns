package iwona.pl.builder;

import java.util.ArrayList;
import java.util.Scanner;

public class TestDrive {
    public static void main(String[] args) {

        final Scanner scanner = new Scanner(System.in);
        final String geekName = scanner.nextLine();
        scanner.close();

        System.out.println("Geek " + geekName + " created.");

        Geek geek = new Geek.GeekBuilder()
                .setType("Amin")
                .setLanguage(new ArrayList<>() {{
                    add("Java");
                    add("SQL");
                }})
                .setExperience(10)
                .build();

        System.out.println(geek);
    }
}
