package iwona.pl.commandForexBroker;

public class SellCommand implements Command {

    private Option option;

    public SellCommand(Option option) {
        this.option = option;
    }

    @Override
    public void execute() {
        option.sell();
    }
}
