package iwona.pl.commandForexBroker;

/*
this is a receiver
 */
public class Option {

    private int amount;

    public Option (int amount){
        this.amount = amount;
    }

    public void buy() {
        System.out.println(amount + " was bought");
    }

    public void sell() {
        System.out.println(amount +" was sold");
    }
}
