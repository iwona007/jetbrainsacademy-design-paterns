package iwona.pl.commandForexBroker;
/*
Suppose you are building a Forex Broker application. Forex broker has two commands which are buy and sell.
The user gives an input sequence which is the amount of money(Option) to be bought and sold. If the user has
given a positive number it is to be bought. if the user has given a negative number, that amount should be sold.
Use the following guidelines.

    Don't change the provided code.
    if the user amount is X> 0, the X amount should be bought. buy command will print X was bought.
    if the user amount is X< 0, the X amount should be sold. sell command will print X was sold.
    The user will give only three inputs.
Sample Input 1: 5 -8 10
Sample Output 1: 5 was bought, -8 was sold, 10 was bought
 */


public interface Command {

    void execute();

}
