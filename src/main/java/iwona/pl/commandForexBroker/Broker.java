package iwona.pl.commandForexBroker;


/*
this is invoker
 */
public class Broker {

    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void executeCommand(){
        command.execute();
    }

}
