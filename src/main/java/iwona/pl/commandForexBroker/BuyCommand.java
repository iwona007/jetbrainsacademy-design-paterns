package iwona.pl.commandForexBroker;

public class BuyCommand implements Command {

    private Option option;

    public BuyCommand(Option option) {
        this.option = option;
    }

    @Override
    public void execute() {
        option.buy();
    }
}
