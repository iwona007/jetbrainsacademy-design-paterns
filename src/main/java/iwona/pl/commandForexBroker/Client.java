package iwona.pl.commandForexBroker;

import java.util.Scanner;

public class Client {
    public static void main(String[] args) {

        Broker invoker = new Broker();

        Command buyCommand;
        Command sellCommand;
        int[] amountList = new int[3];

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i <3 ; i++) {
            amountList[i] = scanner.nextInt();
        }
        for (int i = 0; i <3 ; i++) {
            Option receiver = new Option(amountList[i]);
          if(amountList[i] > 0){
              buyCommand = new BuyCommand(receiver);
              invoker.setCommand(buyCommand);
              invoker.executeCommand();
//              buyCommand.execute();
          } else {
              sellCommand = new SellCommand(receiver);
              invoker.setCommand(sellCommand);
              invoker.executeCommand();
//              sellCommand.execute();
          }
        }
    }
}
