package iwona.pl.subway;

import java.util.Scanner;

public class TestDrive {
    public static void main(String[] args) {

        final Scanner scanner = new Scanner(System.in);
        System.out.println("Hello, which bun would like to?");
        String bun = scanner.nextLine();

        System.out.println("How much salad would like to add?");
        int salad = Integer.valueOf(scanner.nextLine());

        System.out.println("How much cheese would like to add?");
        int cheese = Integer.valueOf(scanner.nextLine());

        System.out.println("How much cucumber would like to add?");
        int cucumber = Integer.valueOf(scanner.nextLine());

        System.out.println("How much ham would like to add?");
        int ham = Integer.valueOf(scanner.nextLine());

        SubwaySandwich subwaySandwich = new SubwaySandwich.Builder()
                .addBun(bun)
                .addSalad(salad)
                .addCheese(cheese)
                .addCucumber(cucumber)
                .addHam(ham)
                .build();
        System.out.println(subwaySandwich);
        scanner.close();
    }

}
