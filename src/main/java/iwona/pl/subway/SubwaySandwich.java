package iwona.pl.subway;

public class SubwaySandwich {
    private String bun;
    private int salad;
    private int cheese;
    private int cucumber;
    private int ham;

    public SubwaySandwich(String bun, int salad, int cheese, int cucumber, int ham) {
        this.bun = bun;
        this.salad = salad;
        this.cheese = cheese;
        this.cucumber = cucumber;
        this.ham = ham;
    }

    public static class Builder {
        private String bun;
        private int salad;
        private int cheese;
        private int cucumber;
        private int ham;

        public Builder addBun(String bun) {
            this.bun = bun;
            return this;
        }

        public Builder addSalad(int salad) {
            this.salad = salad;
            return this;
        }

        public Builder addCheese(int cheese) {
            this.cheese = cheese;
            return this;
        }

        public Builder addCucumber(int cucumber) {
            this.cucumber = cucumber;
            return this;
        }

        public Builder addHam(int ham) {
            this.ham = ham;
            return this;
        }

        public SubwaySandwich build(){
            return new SubwaySandwich(bun, salad, cheese, cucumber, ham);
        }
    }

    @Override
    public String toString() {
        return "SubwaySandwich{" +
                "bun='" + bun + '\'' +
                ", salad=" + salad +
                ", cheese=" + cheese +
                ", cucumber=" + cucumber +
                ", ham=" + ham +
                '}';
    }
}
