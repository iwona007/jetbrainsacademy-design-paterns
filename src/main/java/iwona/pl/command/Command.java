package iwona.pl.command;


/*
Suppose you are creating a stockbroker application. It performs two commands which are buy and sell.
 Use the command pattern to implement this application. Use the following guidelines.
    Don't change the provided code.
    Sell command will print Stock was sold
    buy command will print Stock was bought
    First, it will perform a buy command then it will perform sell command.

 Sample Output 1:
Stock was bought
Stock was sold
 */
public interface Command {
    /* write your code here */
    void execute();
}
