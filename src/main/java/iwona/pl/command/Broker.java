package iwona.pl.command;

/**
 * this is Invoker class
 */
public class Broker {

    private Command command;

    public void setCommand(Command command){
        this.command = command;
    }

    public void executeCommand(){
        command.execute();
    }
}
