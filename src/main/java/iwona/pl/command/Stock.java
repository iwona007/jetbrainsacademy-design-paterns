package iwona.pl.command;

/**
 * This is the receiver.
 */
public class Stock {

    public void buy() {
        System.out.println("Stock was bought");
    }

    public void sell() {
        System.out.println("Stock was sold");
    }
}
