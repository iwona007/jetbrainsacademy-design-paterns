package iwona.pl.command;

/**
 * this is a concrete classes 1
 */
public class BuyCommand implements Command {

    private Stock stock;

    public BuyCommand(Stock stock) {
        this.stock = stock;
    }

    @Override
    public void execute() {
        stock.buy();
    }
}
