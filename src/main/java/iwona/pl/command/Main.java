package iwona.pl.command;

/**
 * This is the client.
 */
public class Main {
    public static void main(String[] args) {


    Broker invoker = new Broker();
    Stock receiver = new Stock();

    Command buyCommand = new BuyCommand(receiver);
    Command sellCommand = new SellCommand(receiver);

    invoker.setCommand(buyCommand);
    invoker.executeCommand();

    invoker.setCommand(sellCommand);
    invoker.executeCommand();


    }
}
